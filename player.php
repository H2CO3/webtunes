<?
	if(!isset($_SESSION)) { session_start(); }
	if(trim($_SESSION['uid']) == '') { $_SESSION['uid']='g';}
	
?>
<?
	require('config.php'); // Load configs
?>
<!DOCTYPE html>
<!-- Original design from http://v1.chrisvalleskey.com/dev/itunes/# -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>WebTunes</title>
<link rel="stylesheet" href="css/mmmm.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/jq.css" />

<!-- Load Aurora.js -->
<script src="libaurora/classlist.js"></script>
<script src="libaurora/player.js"></script>
<script src="libaurora/auroraplayer.js"></script>
<script src="libaurora/aurora.js"></script>
<script src="libaurora/mp3.js"></script>
<script src="libaurora/flac.js"></script>
<script src="libaurora/alac.js"></script>
<script src="libaurora/aac.js"></script>
<!-- Load supporting scripts -->
<script src="jquery.min.js"></script>
 <script src="jsplugins/jquery-ui.js"></script>
 <script src="jsplugins/iframe-transport.js"></script>
<script src="jsplugins/jfile.js"></script>
 <script src="jsplugins/jqfd.js"></script>
 <?
 	$locale = ($_COOKIE['WebTunesLocale']!='' ? $_COOKIE['WebTunesLocale'] : 'en');
 	if(file_exists('localization/'.$locale.'.js')) {
	 	$f = fopen('localization/'.basename($locale).'.js', 'r'); $line = fgets($f); fclose($f);
	 	if($line == '//NOTLANG') {
	 		$locale = 'en';
	 	}
 	} else {
	 	$locale = 'en';
 	} 
 	
 	
 	if($_GET['forceLocale'] != '') {
	 	$locale = $_GET['forceLocale'];
 	}
 ?>
 
 <!--
 	Load the core of..
		 _    _        _      _____                          
		| |  | |      | |    |_   _|                         
		| |  | |  ___ | |__    | |   _   _  _ __    ___  ___ 
		| |/\| | / _ \| '_ \   | |  | | | || '_ \  / _ \/ __|
		\  /\  /|  __/| |_) |  | |  | |_| || | | ||  __/\__ \
		 \/  \/  \___||_.__/   \_/   \__,_||_| |_| \___||___/
		                                                     
		                                                     
		   				by vladkorotnev, 2013
		   				
		   				
		   iTunes skin by Chris Valleskey
		   Codecs for FLAC, ALAC, MP3, AAC by Audiocogs
		   Thanks to everyone involved — see the about page
		   Greetz to iT0ny, Alexander Roche, TheOlegFilms, Emil Yangirov and Arien Hikarin
 -->
 <script src="localization/engine.js"></script>
<script src=<? echo htmlspecialchars("localization/$locale.js"); ?>></script>
<script src="js/player.js"></script>
<script src="js/dragdrop.js"></script>
<script src="js/ui.js"></script>
<script src="js/soundcloud.js"></script>
<script src="js/playlist.js"></script>
<script src="js/tunes.js"></script>
<script src="js/init.js"></script>

<script type="text/javascript">

<?
/* Read passed parameters */
	echo 'window.uid = "'.htmlspecialchars($_SESSION['uid']).'";';
		
		if($_GET['kind'] == 'album') {
			echo ' window.starterUI="album"; window.starterAlbum="'.htmlspecialchars($_GET['album']).'"; ';
		}
		if($_GET['kind'] == 'user') {
			echo ' window.starterUI="user"; window.starterUser="'.htmlspecialchars($_GET['user']).'"; ';
		}
		if(intval($_GET['track']) > 0) {
			echo 'window.starterTrack="'.intval($_GET['track']).'";';
		}
		if(intval($_GET['list']) > 0) {
			echo 'window.starterUI="'.intval($_GET['list']).'";';
		}
	if(trim($scsec) == '' || trim($sckey) == '') {
		echo 'window.soundcloud = false;';
	} else {
		echo 'window.soundcloud = true;';
	}
	
?>
</script>
<style type="text/css"></style></head>
<body style="overflow: hidden">
<!-- IFrame for Downloads -->
<iframe src="" id="f" style="display:none"></iframe>
<header>
<h1 id="apptitle" onclick="about()">__APPNAME__</h1>
<!-- Lang Picker -->
<select id="langpicker" onchange="changeLang()" class="langpick">
	<?
		// This block will look for lang files inside the localization folder
		$files = glob('localization/*.{js,JS}', GLOB_BRACE);
		sort($files);
		foreach($files as $file) {
			$f = fopen($file, 'r'); $line = fgets($f); fclose($f);
			if($line != '//NOTLANG' && substr($line, 0,7) == '//LANG:') {
				$c = 0;
				$langData = str_replace("\n", '', str_replace('//LANG:', '', $line, &$c));
				if($c > 0) {
					// Parse ok
					$langDescriptor = split(':', $langData);
					$langCode = $langDescriptor[0];
					$langName = $langDescriptor[1];
					echo '<option value="'.$langCode.'"';
					if($langCode == $locale) { echo ' selected'; }
					echo '>'.$langName.'</option>';
				}
			}
		}
	?>
</select>
<!-- Upload file thing -->
<input id="fileupload" type="file" style="display:none" name="file" data-url="upload.php">
<!-- Add album form -->
<div id="dialog-form" title="Create new album">
  <p class="validateTips">All form fields are required.</p>
  <form>
  <fieldset>
    <label id="loc_alb_lbl" for="album">Album</label>
    <input type="text" name="album" id="album" class="text ui-widget-content ui-corner-all" />
    <label id="loc_art_lbl" for="artist">Artist</label>
    <input type="text" name="artist" id="artist" value="" class="text ui-widget-content ui-corner-all" />
    <label id="loc_comp_lbl" for="isCompilation">Compilation</label>
    <input type="checkbox" name="isCompilation" id="isCompilation" />
    <input type="hidden" name="zipname" id="zipname" value="">
  </fieldset>
  </form>
</div>
<?
	// If admin, add symlink forms too //
	if($_SESSION['uid'] == 'a') {
		echo '<div id="linkalb-form" title="Link new album">
		  <p class="validateTips">All form fields are required. This will link an album from other folder in here.</p>
		  <form>
		  <fieldset>
		    <label for="album-sy">Album</label>
		    <input type="text" name="album-sy" id="album-sy" class="text ui-widget-content ui-corner-all" />
		    <label for="artist-sy">Artist</label>
		    <input type="text" name="artist-sy" id="artist-sy" value="" class="text ui-widget-content ui-corner-all" />
		    <label for="folder-sy">Folder</label>
		    <input type="text" name="folder-sy" id="folder-sy" value="" class="text ui-widget-content ui-corner-all" />
		    <label for="isCompilation-sy">Compilation</label>
		    <input type="checkbox" name="isCompilation-sy" id="isCompilation-sy" />
		 
		  </fieldset>
		  </form>
		</div>';
	echo '<div id="linktrk-form" title="Link new track">
		  <p class="validateTips">All form fields are required. This will link an a track from other folder in here.</p>
		  <form>
		  <fieldset>
		   <label for="file-st">File</label>
		    <input type="text" name="file-st" id="file-st" value="" class="text ui-widget-content ui-corner-all" />
		 
		  </fieldset>
		  </form>
		</div>';
	}
	
?>
<!-- The thing for popMessage(t) -->
<div class="pop" id="popup"><center><span class="poptext" id="popupt">Hello world</span></center></div>
<!-- Head -->
<section id="head">
	<section id="controls-cell"> <!-- Control buttons -->
		<section class="controls">&nbsp;
			<a href="#" class="btn small prev" onclick="prev()"><span>◀◀</span></a>
			<a href="#" class="btn play" onclick="playPause()"><span id="btn-play">▶</span><span id="btn-pause" style="display:none"><img src="img/pause.png" class="pause-blob"></span></a>
			<a href="#" class="btn small next" onclick="next()"><span>▶▶</span></a>
		</section>
	</section>
	<section id="volume-cell"><!-- Volume bar -->
		<section class="volume">&nbsp;
			<div class="volume-bar">
				<span class="volume-bar-knob"></span>
			</div>
		</section>
	</section>
	<section id="display-cell"> <!-- Display -->
		<section class="display">
		<div class="track-left" id="sharer"><a href="#" id="loc_sharer" onclick="shareTune()" title="Share this tune"><img src="img/share-blob.png"></a></div>
			<section class="track-info">
				
				<div id="displ" ><h2 id="song-title">Song Title</h2>
				<div class="album-band">
					<h3 id="song-album">Album Title</h3>
					<h3 id="song-band">Band Title</h3>
				</div>
				
				<div class="track-position">
				<div class="track-position-time">--:--</div>
				<div class="track-progress-cell"><div class="track-progress"><span  class="track-progress-knob">◆</span></div></div>
				<div class="track-position-remaining">--:--</div>
				</div></div>
				
			</section>
	  <div class="track-right" id="keeper" style=""><a href="#" id="loc_keeper" onclick="keepTune()" title="Download this tune"><img src="img/down-blob.png"></a></div>
		</section>
	</section>
	<section id="search-cell">
	<!-- Audio tag for fallback purposes (unused?) -->
	<audio id="audiotag" preload="auto"></audio>
	
	<?
	echo '<center>';
	
	if($_SESSION['uid'] == 'a') {
		// Admin stat panel :3
		echo '<a id="userctl" href="usercontrol"><small>[__USEREDIT__]</small></a><br>';
		echo '<a href="#" id="symimport" onclick="symlinkImport()"><small>[__SYMIMPORT__]</small></a><br>';
		echo '<a href="#" id="symtrack" onclick="symlinkTune()"><small>[__SYMTRACK__]</small></a><br>';
		echo '<span id="disk" onclick="disk()">__DSPACE__: '.round(disk_free_space('useralbums') / 1024 / 1024).' MB</span>';
		
	}
	echo '</center>';
	?>
	<!-- <input type="text" id="s" name="s" placeholder="Search"> -->
	</section>
</section>
</header>
<div id="sideplace">

</div>
<section id="main">
	<ul class="list">
		<li class="head" style="display:none">
			<span class="song-number">&nbsp;</span>
			<span class="song-name">Name</span>
			<span class="song-time">Time</span>
			<span class="song-artist">Artist</span>
			<span class="song-album">Album</span>
		</li>
	</ul>
	<div class="scroll" id="listing-container">
		<div class="cloudsearch" id="csearch">
		<input type="search" id="scsearchfield" placeholder="Search on SoundCloud" class="cloudsearchfield" onkeydown="if (event.keyCode == 13) { searchCloud(); return false; }">
			
		</div>
		<ul id="listing" class="list">
			<!-- The list of tracks will go here by ajax -->
		</ul>
	</div>
</section>
<footer> <!-- Bottom bar -->
<section class="footer-left">
<a href="#" class="btn-footer first" onclick="about()"><span id="loc_about">About</span></a>
<a href="#" class="btn-footer" id="shu" onclick="shuffle()"><span id="loc_shuffle">Shuffle</span></a>
<? if(isUser()) { 
echo '<a href="logoff.php" class="btn-footer last"><span id="loc_logoff">Logoff</span></a>'; 
} else {
echo '<a href="index.php" class="btn-footer last" ><span id="loc_logon">Login</span></a>'; 
}
 ?> 
</section>
<section class="footer-right">
<a href="#" class="btn-footer first single" id="trash" onclick="trashList()" ondragover="allowDropTrash(event)" ondragend="noDropTrash(event)" ondragleave="noDropTrash(event)" ondrop="dropToTrash(event)"><img class="footer-icon" src="img/trash-blob.png"></a>
<a href="#" class="btn-footer first single" id="renamelist" onclick="renameList()">Rename</a>
<a href="#" class="btn-footer single " id="sharelist" onclick="shareList()">Share</a>
<!--<a href="#" class="btn-footer single" onclick="about()">About</a>-->
</section>
<section class="footer-center">
<center id="footer-process"><div id="progressbar"> <!--Progress bar-->
  <div id="progressbar-footer" style="float:left"></div>
</div><small id="process-sig">Prebuffering...</small></center>
</section>
</footer>

</body></html>