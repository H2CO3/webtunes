<?php
/* Get "Unsorted" Music */
require('config.php');

function escapeShit($str) {
	return str_replace("'", "\\'", $str);
}
// include getID3() library (can be in a different directory if full path is specified)
require_once('getid3/getid3.php');
if(!isUser()) { die('');}
// Initialize getID3 engine
$getID3 = new getID3;

$DirectoryToScan = 'usertracks/'.intval($_SESSION['uid']); // change to whatever directory you want to scan
$dir = opendir($DirectoryToScan);
$iterator = 1;
while ($files[] = readdir($dir));
sort($files);
closedir($dir);

foreach ($files as $file) {
	$FullFileName = realpath($DirectoryToScan.'/'.$file);
	if ((substr($FullFileName, 0, 1) != '.') && is_file($FullFileName)) {
		set_time_limit(30);

		$ThisFileInfo = $getID3->analyze($FullFileName);

		getid3_lib::CopyTagsToComments($ThisFileInfo);
		
		$fname=$ThisFileInfo['filename'];
		$artist=$ThisFileInfo['comments_html']['artist'][0];
		$title = $ThisFileInfo['comments_html']['title'][0];
		$album = $ThisFileInfo['comments_html']['album'][0];
		$time=$ThisFileInfo['playtime_string'];
		if(trim($title) == '') { $title = $fname; }
		// output desired information in whatever format you want
		/* <li class="" onclick="loadFile('tests/aac.m4a')">
				<span class="song-number">1</span>
				<span class="song-name">Dar, Unde Esti</span>
				<span class="song-time">AAC</span>
				<span class="song-artist">O-Zone</span>
				<span class="song-album">DiscO-Zone</span>
			</li>
		*/
		$ext =strtoupper(end( explode('.', $fname)));
		echo "<li class=\"song\" id=\"".$DirectoryToScan."/".$fname."\" draggable=\"true\" ondragstart=\"drag(event)\" onclick=\"loadFile('".$DirectoryToScan."/".escapeShit($fname)."')\">";
		echo '<span class="song-number"><span  class="format" id="'.$DirectoryToScan."/".$fname.'_format">'.$ext.'</span><img src="img/now.png" class="now" id="'.$DirectoryToScan."/".$fname.'_ico"></i> '.$iterator.'</span>
				<span class="song-name">'.$title.'</span>
				<span class="song-time">'.$time.'</span>
				<span class="song-artist">'.$artist.'</span>
				<span class="song-album">'.$album.'</span>';
		echo "</li>";
		$iterator+=1;
	}
}
