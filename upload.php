<?
// File upload
require('config.php');
if(!isUser()) { die('disallow');}
if(!isset($_SESSION)) { session_start();}
$uid = intval($_SESSION['uid']);
$uploaddir = 'upload/';
$uploadfile = $uploaddir . basename($_FILES['file']['name']);
ini_set('upload_max_filesize', 2147483648);
$audio = array('mp3','wav','flac','m4a');
// If single file audio, move to user's unsorted folder
if(in_array(strtolower(pathinfo($uploadfile, PATHINFO_EXTENSION)), $audio) ) {
	@mkdir('usertracks/'.intval($uid),0777,true);
	if (move_uploaded_file($_FILES['file']['tmp_name'], 'usertracks/'.intval($uid).'/'.str_replace('\'', '', basename($_FILES['file']['name'])))) {
    	echo "All OK";
	} else {
	    echo "ERROR ".$_FILES['file']['error'];
	   
	}

}
// If a zip, process it
if(strtolower(pathinfo($uploadfile, PATHINFO_EXTENSION)) == 'zip' ) {
	if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
		$zip = new ZipArchive;

    	if ($zip->open($uploadfile) === TRUE) {
    	$dir = 'upload/'.md5($uploadfile);
    		mkdir($dir, 0777, true);
    		$toGetOut = array();
    		$fs = '';
    		for( $i = 0; $i < $zip->numFiles; $i++ ){ 
			    $stat = $zip->statIndex( $i ); 
			    $fname = basename( $stat['name'] ) ;
			      
			    if(in_array(pathinfo($fname, PATHINFO_EXTENSION), $audio) && !(substr($fname, 0, 2) === '._' )) {
				    array_push($toGetOut, $fname); // Extract only audio file and omit MacShit
				  $fs.=$fname."\n";
			    }
			}
			  $zip->extractTo($dir.'/', $toGetOut); // Perform extraction
			  $zip->close();
			  
			 echo 'ZIP:'.md5($uploadfile).':'.$fs; // pass param back to JS
		} else {
			  echo 'ERROR'; // pass error to JS
		}
		unlink($uploadfile);
	} else {
	    echo "ERROR ".$_FILES['file']['error'];
	}
	
	die();
}

die('Error uploading '.basename($_FILES['file']['name']));



?>