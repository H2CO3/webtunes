function popMessage(msg) { // show notification
	document.getElementById('popupt').innerHTML=msg;
	document.getElementById('popup').style.top='0';
	setTimeout(unPop,2000);
}
function unPop() { // hide notification
	document.getElementById('popup').style.top='-80px';
}

window.curUI = '';
var hasSwitched=false;
var displayBandTimeout=undefined;
var cachedUIs = new Object(); // Cache listings for snappier UI

function writeUI(data, identifier) {
	// This will write the UI to cache, and then present it in the viewport
	cachedUIs[identifier] = data;
	$('#listing').html(data);
}
function appendUI(data, identifier) {
	// this will append to cached ui and display
	cachedUIs[identifier] = cachedUIs[identifier] + data;
	if(window.curUI == identifier) {
		$('#listing').append(data);
	}
	
}

function invalidateUI(identifier) {
	// this will force a ui to reload next time it's called
	cachedUIs[identifier] = undefined;
	if(window.curUI == identifier) {
		switchTo(identifier);
	}
}

function useCachedUI(identifier) {
	// this will pop up a cached ui
	$('#listing').html(cachedUIs[identifier]);
}

function loadSidebar() { //ajax load sidebar
$('#sideplace').empty();
	$.ajax({type:"POST", url:'renderSidebar.php', success:function(data) {
				$('#sideplace').html(data);
				localizeSidebar();
				if(window.curUI != undefined) {
					switchTo(window.curUI);
				}
			}});

}

function showSect (sect) {
	
	$('#'+sect)[0].style.height = '';
	 $('#'+sect)[0].style.display = '';
	 $('#'+sect)[0].style.opacity = '1'; 
}
function hideSect (sect) {
	$('#'+sect)[0].style.opacity = '0'; 
	setTimeout(function() {$('#'+sect)[0].style.height = '0'; $('#'+sect)[0].style.display = 'none'; } , 500);
}
function toggleSect(sect) {
	if($('#'+sect)[0].style.display == 'none') {
		showSect (sect);
	} else {
		hideSect (sect);
	}
}
function switchTo(itm) {
	// Present a view selected on sidebar
	if(window.curUI != itm) {
		hasSwitched=true;
		window.curUI=itm;
	}
	for(var a=0; a < $('.lib').length; a++) { $('.lib')[a].className='lib'; } // deselect all library items
	try {
		document.getElementById(itm).className='lib current'; // select the one we are presenting
	} catch(e) {
	
	}
	if(itm.substr(0, 2) == 'sc') {
		document.getElementById('renamelist').style.display='none'; // hide buttons on soundcloud
	document.getElementById('sharelist').style.display='none';
	} else {
		document.getElementById('renamelist').style.display='inline-block';
		document.getElementById('sharelist').style.display='inline-block';
	}
	if(cachedUIs[itm] != undefined) {
		console.log("Using cached "+itm);
		useCachedUI(itm);
		return;
	}
	if(itm.substring(0, 3) === 'alb') {
		showOneAlbum(itm.split('alb')[1]); // show album if it was album
		return;
	}

	
	if(itm.substr(0, 4) == "scs_") {
		// The search routine will handle this for us, and the next times the cache will do as well
		return;
	}
	if(itm.substr(0, 4) == "scl_") {
		loadCloudList(itm.substr(4, itm.length));
		
		return;
	}
	
	$('#listing').empty();
	
	switch (itm) {
			case 'sc_search': 
				 $('#scsearchfield').focus();
			break;
			
			
		case "music": // download user music
		$.ajax({type:"POST", url:'getUserAlbums.php', contentType:"application/x-www-form-urlencoded", success:function(data) {
				writeUI(data, 'music');
				
			}});
			break;
			
		case "compilation":
		$.ajax({type:"POST", url:'getUserAlbums.php', contentType:"application/x-www-form-urlencoded", data:{"compilations":"1"} , success:function(data) {
				writeUI(data, 'compilation');
				
			}});
			break;
			
		case "unsorted":
			
			$.ajax({type:"POST", url:'getUserTracks.php', contentType:"application/x-www-form-urlencoded", data:{"user":"0"} , success:function(data) {
				writeUI(data, 'unsorted');
					
			}});
			break;
			
			
		default:
			if(itm.substr(0, 3) != 'sc_') { // if plalist not soundcloud, use playlist display
				loadList(itm);
			} else {
				loadCloudUI(itm);
			}
			break;
	}


}

function loadList(itm) {
	$.ajax({type:"POST", url:'getPls.php', contentType:"application/x-www-form-urlencoded",  data:{"user":"0", "list":itm} ,   success:function(data) {
				writeUI(data, itm);
					if(window.starterUI !=itm) {
						document.getElementById('renamelist').style.display='inline-block';
						document.getElementById('sharelist').style.display='inline-block';
					} else {
						document.getElementById('renamelist').style.display='none';
						document.getElementById('sharelist').style.display='inline-block';
					}
					
				if(window.starterTrack != undefined) { 
					loadFile($('.song')[window.starterTrack-1].id, false);
					window.starterTrack=undefined;
					}	
				}});
}

function showOneAlbum(id) { // Show a single album in listing
try {
	for(var a=0; a < $('.lib').length; a++) { $('.lib')[a].className='lib'; }
	document.getElementById('alb'+id).className='lib current';
} catch(e) {
	
}
	
	$('#listing').empty();
	hasSwitched=true;
		
			
			$.ajax({type:"POST", url:'getOneAlbum.php', data:{"album":id} , success:function(data) {
				writeUI(data, 'alb'+id);
				if(window.starterTrack != undefined) { 
					loadFile($('.song')[window.starterTrack-1].id, false);
					window.starterTrack=undefined;
				}	
			}});


}

function showUser(itm) { // Show a user's unsorted in listing
	for(var a=0; a < $('.lib').length; a++) { $('.lib')[a].className='lib'; }
	$('#listing').empty();
	hasSwitched=true;
		
			
			$.ajax({type:"POST", url:'getUserTracksC.php', data:{"c":itm} , success:function(data) {
				writeUI('usr'+data);
				if(window.starterTrack != undefined) { 
					loadFile($('.song')[window.starterTrack-1].id, false);
					window.starterTrack=undefined;
				}	
			}});


}

function showBand() { // animate to band line on display
	clearTimeout(displayBandTimeout);
	$('#song-album')[0].style.top="-13px";
	$('#song-album')[0].style.opacity="0";
	$('#song-band')[0].style.top="-13px";
	$('#song-band')[0].style.opacity="1";
	displayBandTimeout=setTimeout(showAlbum, 5000);
}
function showAlbum() { // animate to album line on display
	clearTimeout(displayBandTimeout);
	$('#song-album')[0].style.top="0px";
	$('#song-album')[0].style.opacity="1";
	$('#song-band')[0].style.top="0px";
	$('#song-band')[0].style.opacity="0";
	displayBandTimeout=setTimeout(showBand, 5000);
}

function disk() { // Disk space refreshener
	if(window.uid != 'a') return;
	$('#disk').empty();
	$.ajax({type:"POST", url:'diskspace.php', success:function(data) {
								$('#disk').html(window.locale.ui.diskSpace + data);
					}});

}

 
function about() {
	invalidateUI('about');
	$.ajax({type:"GET", url:'about.php', success:function(data) {
								writeUI(data+window.locale.credit, 'about');
								switchTo('about');
					}});
	
}


function changeLang() {
	$.ajax({type:"GET", url:'setLang.php', data:{"loc":$('#langpicker').val()}, success:function(data) {
								window.location.href = window.location.href.split('#')[0].split('?')[0];
					}});
}

