function loadCloud(url, manually) {
		console.log("Aw shit its SC! Fall back to audio for the time being");
		window.audio = new Audio();
		var tid=url.substr(2, url.length);
		window.audio.src = "sc_auth.php?act=stream&tid="+tid; // load from stream proxier
		window.audio.play();
		window.audio.oncanplay=(function() {
			hadstarted=true;
			document.getElementById('btn-pause').style.display="block";
			document.getElementById('btn-play').style.display="none";
			window.audio.volume = $( ".volume-bar" ).volslider('option','value') / 100;
		});
		window.prb = 0;
		window.audio.addEventListener("progress", function(a)
			  {
			  console.log('buf ');
			  window.prb++;
			  document.getElementById('progressbar-footer').style.width=window.prb.toString()+"%";
			  document.getElementById('footer-process').style.display="block";
			  document.getElementById('process-sig').innerHTML=window.locale.ui.cloudStream;
			  }
			);
		window.audio.addEventListener("stalled", function()
			  {
			  popMessage(window.locale.ui.bufferingStalled);
			  }
			);
		window.audio.addEventListener("canPlay", function()
			  {
				  hadstarted=true;
				document.getElementById('btn-pause').style.display="block";
				document.getElementById('btn-play').style.display="none";
				window.audio.volume = $( ".volume-bar" ).volslider('option','value') / 100;
				window.audio.play();
			  }
			);
			window.audio.addEventListener("ended", function(e)
			  {
			  	next();
			  }
			);
			hadstarted=true;
			document.getElementById('btn-pause').style.display="block";
			document.getElementById('btn-play').style.display="none";
			window.audio.volume = $( ".volume-bar" ).volslider('option','value') / 100;
			$('#displ')[0].style.opacity="1";
			if(manually) {
				var title = document.getElementById(tid+'_title').innerHTML;
				var author = document.getElementById(tid+'_author').innerHTML;
			} else {
				var title = window.trackNames[url];
				var author = window.trackArtists[url];
			}
			
			
				document.getElementById('song-title').innerHTML=title;
				document.title = window.locale.appname+': '+title+' - '+author;
			
				console.log('Fallback, disabling seek');
			$( ".track-progress" ).seekslider('option','disabled',true);
			$( ".volume-bar" ).volslider('option','disabled', false);
			disableProg();
			document.getElementById('song-album').innerHTML=window.locale.ui.soundCloudBeta;
			document.getElementById('song-band').innerHTML=author+window.locale.ui.onSC; // they asked me to credit them, so this is why
			document.getElementById('sharer').style.display="none";
			document.getElementById('keeper').style.display="table-cell";
			
			clearTimeout(displayBandTimeout);
			showBand();
			for(var a=0; a < $('.now').length; a++) { $('.now')[a].style.display='none'; }
			for(var a=0; a < $('.format').length; a++) { $('.format')[a].style.display='inline-block'; }
			try {
				document.getElementById(url+'_format').style.display="none";
				document.getElementById(url+'_ico').style.display="inline-block";
			} catch(e) {}
}

function moarSC(curs) {
	// load more soundcloud
	$('#SCmoar').empty();
	$('#SCmoar').remove();
	writeUI($('#listing').html(), 'sc_feed');
	popMessage(window.locale.ui.wait);
	$.ajax({type:"GET", url:'sc_auth.php', data:{"act":"moar", cursor:curs} , success:function(data) {
	 			appendUI(data, 'sc_feed');
	 			
			}});
}


function searchCloud () {
	// SoundCloud search handler
	$('#listing').empty();
	var q = $('#scsearchfield').val();
	var sd;
	if($('#scs_'+q.replace(' ', '_')).length <= 0) {
		sd = $('#sc_search').clone(true);
		sd[0].style.display = "";
		sd[0].id = "scs_"+q.replace(' ', '_');
		var urq = q;
		if(urq.length > 16) {
			urq = q.substring(0, 15) + '…';
		}
		sd[0].innerHTML='<img src="img/search.png" class="search-icon"><a href="#" onclick="switchTo(\'scs_'+q+'\')">'+urq+'</a>';
		$('#soundclouds').append(sd);
	} else {
		sd = $('#scs_'+q.replace(' ', '_'));
	}
	
	
	switchTo(sd[0].id);
	$.ajax({type:"GET", url:'sc_auth.php', contentType:"application/x-www-form-urlencoded", data:{"act":"search", "query": q} , success:function(data) {
				writeUI(data, sd[0].id);
				
			}});
	$('#scsearchfield').val('');
}

function loadCloudList(lid) {
	// open playlist from SoundCloud
						document.getElementById('renamelist').style.display='none';
						document.getElementById('sharelist').style.display='none';
	$.ajax({type:"GET", url:'sc_auth.php', data:{"act":"playlist", "lid":lid} ,   success:function(data) {
					writeUI(data, 'scl_'+lid);
					
						}});
}

function loadCloudUI(itm) {
	 // open UI from SoundCloud
	 
						document.getElementById('renamelist').style.display='none';
						document.getElementById('sharelist').style.display='none';
				$.ajax({type:"GET", url:'sc_auth.php', data:{"act":itm.substr(3, itm.length)} ,   success:function(data) {
					writeUI(data, itm);
					
						}});
}
function newSCList() {

	var lname = prompt(window.locale.dialogs.newSClist, window.locale.dialogs.unamedSClist);
	if(lname == undefined) return;
	$.ajax({type:"GET", url:'sc_auth.php', data:{"act":"mklist", "name": lname} ,   success:function(data) {
							loadSidebar();
							console.log(data);					
						}});
	
	
}