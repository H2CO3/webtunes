# tunes.js
## generic track manipulation and management routines

* `symlinkTune()` opens up a single-track symlink dialog, that is used to make a symlink of a track into unsorted or an album. **admin only**
* `symlinkImport()` opens a dialog that is used to symlink a folder on the server as an album. **admin only**
* `keepTune()` is the download track button handler
* `downTune(t)` gets a track downloaded by it's URL/ID through a header-changing script and an iframe (to prevent interruption of playback), or, for now, for SoundCloud tracks, through window.location and a downloader/streamripper script.
* `performZipDecomp(par)` is the thing that calls up the ZIP decompression (album addition) dialog with the specified parameters, where `par` is the colon-separated array returned by the upload script
* `shareTune()` calculates the link for the current track and pops it up for the user to copy