# ui.js
## User interface routines
- - -
### Notifications
* `popMessage(msg)` pops up a little notification on the top
* `unPop` pulls it back
- - - 
### UI routines and vars
* `window.curUI` contains the current UI identifier at the time
* `window.playingFromUI` contains the UI the playback was started from

** Never change the UI by calling `$('#listing').html()` and so on! **

* `writeUI(data, identifier)` writes a UI both to the cache and the viewport
* `appendUI(data, identifier)` appends data to both the viewport and the specified ID
* `invalidateUI(identifier)` removes a UI from the cache and forces it to reload next time it's requested
* `useCachedUI(identifier)` puts a UI from the cache to the viewport
* `loadSidebar` calls renderSidebar.php to make a sidebar for us
* `switchTo(itm)` loads the specified UI
* `showOneAlbum(id)` loads an album by id, and if it was called as a part of init sequence, starts playing the starting track, if that was specified
* `showUser(itm)` shows a user's 'unsorted' by user id
* `showBand()` and `showAlbum()` transition to display the artist or the album respectively
* `disk()` refreshes the admin disk space display
* `about()` is 'About'

### UI ID format
* `music` is for the music tab
* `unsorted` is for the unsorted tab
* `compilations` is for the compilations tab
* `alb<ID number>` is for an album with the specified id number
* `sc_<feed|faves|search>` is for SoundCloud's feed, favorites and search UI respectively
* `scs_<query>` is for a specific search query on SoundCloud. *Please note that switching to that UI using `switchTo` won't start you a search request.*