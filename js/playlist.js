function renameList() { // rename a playlist
	var list=$('.lib.current')[0].id;
	var newN = prompt(window.locale.dialogs.plsRename,  $('.lib.current')[0].children[0].innerHTML);
	if(newN.length > 16) {
		newN = newN.substring(0, 15) + '…';
	}
	$.ajax({type:"POST", url:'renameList.php', data:{ "list":list, "name":newN } , success:function(data) {
				var inf= jQuery.parseJSON(data);
				$('#'+inf['id'])[0].children[0].innerHTML=inf['name'];
				
			}});
}

function shareList() { // share a playlist
	var list=$('.lib.current')[0].id;
	var link=window.location.href.split('#')[0].split('?')[0];
	if(list.substr(0, 2) == 'sc') {
		// No share soundcloud just yet
		return;
	}
	if(list.substr(0, 3) == 'alb') {
		link = link + '?kind=album&album='+list.substr(3, list.length);
	} else {
		link = link + '?list='+list.toString();
	}
	
	prompt(window.locale.dialogs.copyLink,link);
}


var tdellist;
function trashList() { // trash a playlist
	if(window.curUI == 'music' || window.curUI == 'unsorted' || window.curUI == 'compilation' || window.curUI.substr(0, 3) == 'alb' || window.curUI.substr(0, 3) == 'sc_' || window.curUI.substr(0, 3) == 'scs') {
		alert(window.locale.dialogs.trash);
		return;
	}
	var list=$('.lib.current')[0].id;
	tdellist=list;
	for (var i=0; i< $('.lib.current')[0].children.length; i++) {
		if($('.lib.current')[0].children[i].className == 'savebutton') {return;}
	}
	if (confirm(window.locale.dialogs.del1+$('.lib.current')[0].children[0].innerHTML+window.locale.dialogs.del2)) {
		writeUI(window.locale.ui.wait, window.curUI)
		if(list.substr(0, 4) != 'scl_') {
			$.ajax({type:"POST", url:'killlist.php',  data:{"list":list} , success:function(data) {
				$('.lib.current')[0].innerHTML='';
				writeUI('', window.curUI)
				popMessage(window.locale.errors.deleted);
					
			}});
		} else {
			$.ajax({type:"GET", url:'sc_auth.php', data:{"act":"killlist", "lid": list.substr(4, list.length)} ,   success:function(data) {
							if(data == '200 - OK') {
									$('.lib.current')[0].innerHTML='';
										writeUI(window.locale.ui.wait, window.curUI)
									popMessage(window.locale.errors.deleted);
							}		else {
								popMessage(window.locale.errors.fail);
								console.log(data);
							}			
						}});
	
		}
	}
}

function copyList(list) {
	 // Duplicate playlist of other user to our acct
	 $.ajax({type:"POST", url:'copyList.php', data:{"list":list} , success:function(data) {
	 			$('#save'+list)[0].style.display="none";
	 			var ls = $('#'+list)[0];
	 			ls.id = data;
	 			var n = ls.children[1].innerHTML;
	 			
	 			ls.innerHTML='<a href="#" onclick="switchTo(\''+data+'\')">'+n+'</a>';
	 			switchTo(data);
			}});
 }
 
 var tListN=undefined;
function makeList(evt) {
	// new playlist
	if(evt.altKey) {
	 	newSCList();
		return;
	}
	 mkLst();
	 
 }
 
 function mkLst() {
	 tListN = prompt(window.locale.dialogs.newList,window.locale.dialogs.unamedList);
	 if(tListN == undefined) return;
	 
	 $.ajax({type:"POST", url:'mklist.php', data:{"name":tListN} , success:function(data) {
	 			var lawl='<li id="'+data+'" class="lib" ondragover="allowDrop(event)" ondragend="noDrop(event)" ondragleave="noDrop(event)" ondrop="drop(event)"><a href="#" onclick="switchTo(\''+data+'\')">'+tListN+'</a></li>';
	 			$('#playlists')[0].innerHTML = $('#playlists')[0].innerHTML + lawl;
	 			switchTo(data);
			}});
 }
 
function updateProg() {
	// progress update
if(window.player == undefined) return;
	$( ".track-progress" ).seekslider('option','value',(window.player.currentTime / window.player.duration)*1000);
	var cseconds = window.player.currentTime / 1000;
	var cnumminutes = Math.floor((((cseconds % 31536000) % 86400) % 3600) / 60);
	var csminutes = cnumminutes.toString();
	if(cnumminutes < 10) { csminutes = '0'+csminutes; }
	var cnumseconds = Math.floor((((cseconds % 31536000) % 86400) % 3600) % 60);
	var csseconds = cnumseconds.toString();
	if(cnumseconds < 10) { csseconds = '0'+csseconds; }
	$('.track-position-time')[0].innerHTML = csminutes.toString()+':'+csseconds.toString();
	
	var aseconds = (window.player.duration-window.player.currentTime) / 1000;
	var anumminutes = Math.floor((((aseconds % 31536000) % 86400) % 3600) / 60);
	var asminutes = anumminutes.toString();
	if(anumminutes < 10) { asminutes = '0'+asminutes; }
	var anumseconds = Math.floor((((aseconds % 31536000) % 86400) % 3600) % 60);
	var asseconds = anumseconds.toString();
	if(anumseconds < 10) { asseconds = '0'+asseconds; }
	$('.track-position-remaining')[0].innerHTML = '-'+asminutes.toString()+':'+asseconds.toString();
	if(window.player.buffered == 100 && window.player.duration - window.player.currentTime < 1200) { clearTimeout(samplesFix); samplesFix=setTimeout(next, 1000); }
	cseconds=0; aseconds=0;
	
}

function disableProg() {
	$( ".track-progress" ).seekslider('option','value',0);
	$('.track-position-time')[0].innerHTML = "--:--";
	$('.track-position-remaining')[0].innerHTML = "--:--";
}