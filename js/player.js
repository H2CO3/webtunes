//+ Jonas Raoni Soares Silva
//@ http://jsfromhell.com/array/shuffle [v1.0]
function arrayShuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};


function loadFile(url, isNotManual) { // Most hardcore routine
	hadstarted=false;
	curUrl = url;
	window.curFile=url;
	$('#displ')[0].style.opacity=1;
	if(window.player != undefined) {
		window.player.asset.stop();
		window.player.stop();
		window.player = undefined;
	}
	if(window.audio != undefined) {
		window.audio.pause();
		window.audio = undefined;
	}
	if(url.substr(0, 2)!= 'SC') {
		
		loadCogs(url);
	
	} else {
		loadCloud(url, !isNotManual);
	}
	if(isNotManual == true) return;
	buildQueue(url);
 }

var curUrl='';

window.playingFromUI='';
window.curFile='';
// ---- For SoundCloud Fallback Only ----
window.trackNames = new Object;
window.trackArtists = new Object;
// --------------------------------------
 window.queue=new Array;
 window.curQueueItem=-1;
 var updater=null;	var samplesFix = null; 

function buildQueue(url) {
	
	if(hasSwitched) {
		window.queue = new Array;
	}
		window.playingFromUI=window.curUI;

	window.curQueueItem=-1;
	var canAdd=true;
	var iterator=0;
		if (hasSwitched) {
			for(var a=0; a < $('.song').length; a++) { // calculate queue
					window.queue.push($('.song')[a].id);
					
					if(window.playingFromUI.substr(0, 2) == 'sc') {
						// If SoundCloud, make a dictionary of all users and tracks in playlist since there isnt a way to get metadata for it
						try {
							window.trackNames[$('.song')[a].id] = $('.song')[a].children[1].innerHTML;
							window.trackArtists[$('.song')[a].id] = $('.song')[a].children[3].innerHTML;
						} catch(e) {}
					}
			}
		}
			window.curQueueItem=window.queue.indexOf(url);
		hasSwitched=false;
		if(window.isShufflin) {
			shuffleQ();
		}
}

function playPause() {
	if(window.player == undefined && window.audio == undefined) return;
	if(window.player != undefined) {
		if(window.player.playing) {
			window.player.pause();
			document.getElementById('btn-pause').style.display="none";
			document.getElementById('btn-play').style.display="block";
		} else {
			window.player.play();
			document.getElementById('btn-pause').style.display="block";
			document.getElementById('btn-play').style.display="none";
		}
	}
	if(window.audio != undefined) {
		if(!window.audio.paused) {
			window.audio.pause();
			document.getElementById('btn-pause').style.display="none";
			document.getElementById('btn-play').style.display="block";
		} else {
			window.audio.play();
			document.getElementById('btn-pause').style.display="block";
			document.getElementById('btn-play').style.display="none";
		}
	}
}



function stop() {

	if(window.player == undefined && window.audio == undefined) return;
	if(window.player != undefined) window.player.stop();
	if(window.audio != undefined) window.audio.pause(); 
	clearTimeout(displayBandTimeout);
	document.getElementById('btn-pause').style.display="none";
	document.getElementById('btn-play').style.display="block";
	$('#displ')[0].style.opacity="0";
	for(var a=0; a < $('.now').length; a++) { $('.now')[a].style.display='none'; }
}

function next() {
	if(window.player != undefined) if(!window.player.playing) return;
	if (window.queue == undefined || window.queue.length == 0) { alert(window.locale.errors.queueIsUndefined); return; }
	window.curQueueItem++;
	if(window.curQueueItem >= window.queue.length) {
		
		$('#displ')[0].style.opacity="0";
		stop();
		window.curQueueItem=0;
	
		return;
	}
	loadFile(window.queue[window.curQueueItem],true);
	
}
function prev() {

	if(window.player != undefined) if(!window.player.playing) return;
	if(window.audio != undefined) if(window.audio.paused) return;
	clearTimeout(samplesFix);
	if (window.queue == undefined || window.queue.length == 0) { alert(window.locale.errors.queueIsUndefined); return; }
	window.curQueueItem--;
	if(window.curQueueItem < 0) {
		$('#displ')[0].style.opacity="0";
		window.curQueueItem=window.queue.length-1;
		stop();
		
		return;
	}
	loadFile(window.queue[window.curQueueItem],true);
	
}


function loadCogs(url) {
	// Not soundcloud, go on with Audiocogs
		if(window.newplayer != undefined)
		{
			window.player = undefined;
			window.player = window.newplayer;	
		} else {
			window.player = AV.Player.fromURL(url);
		}
		// Preload file off url
		window.player.preload();
		window.player.asset.on('format', function() {
			window.player.asset.get('metadata', function(m) { // Got metadata, display it
				$('#displ')[0].style.opacity="1";
				var f = window.player.asset.source.url.split('.');
				var ext = f[f.length-1];
				if(m.title == undefined || m.title == '' || ext.toLowerCase() == 'wav') {
					var a = window.player.asset.source.url.split('/');
					document.getElementById('song-title').innerHTML=a[a.length-1];
					document.title = window.locale.appname+': '+a[a.length-1];
				} else{
					document.getElementById('song-title').innerHTML=m.title;
					document.title = window.locale.appname+': '+m.title+' - '+m.artist;
				}
				if(ext.toLowerCase() == 'flac') {
					console.log('Flac, disabling seek');
					$( ".track-progress" ).seekslider('option','disabled',true);
				} else {
					console.log('Not flac, enabling seek');
					$( ".track-progress" ).seekslider('option','disabled',false);
				}
				
				document.getElementById('song-album').innerHTML=m.album;
				document.getElementById('song-band').innerHTML=m.artist;
				clearTimeout(displayBandTimeout);
				showBand();
		  	});
		});
		
			
		window.player.asset.on('buffer', function(p) { // Buffering progress
			document.getElementById('progressbar-footer').style.width=p.toString()+"%";
			if(p == 100) {
				document.getElementById('footer-process').style.display="none";
				
				$('body')[0].style.cursor = "default";
			} else {
				document.getElementById('progressbar-footer').style.width=p.toString()+"%";
				document.getElementById('footer-process').style.display="block";
				document.getElementById('process-sig').innerHTML=window.locale.ui.buffering;
				$('body')[0].style.cursor = "progress";
			}
			if(p >= 25 && !hadstarted) { 
			 window.player.play();  // buffered enough and not playing yet, play
			 hadstarted=true;
				document.getElementById('btn-pause').style.display="block";
			document.getElementById('btn-play').style.display="none";
			 }
		});
		
		window.player.on('progress', function(pr) {
			updateProg(); // update progress
		});
		
		window.player.on('error', function(e) {
			popMessage(window.locale.errors.playError+': '+e);
			next();
		});
		
		window.player.volume = $( ".volume-bar" ).volslider('option','value'); // set volume
		for(var a=0; a < $('.now').length; a++) { $('.now')[a].style.display='none'; } // update icons
		for(var a=0; a < $('.format').length; a++) { $('.format')[a].style.display='inline-block'; }
		try {
			document.getElementById(url+'_format').style.display="none";
		document.getElementById(url+'_ico').style.display="inline-block";
		} catch(e) {}
		// calculate album id
		window.curAlbumID =  $(document.getElementById(url)).find('.song-album')[0].id.split('album')[1];
		for (var i=0; i< $('.album'+window.curAlbumID).length; i++ ){
			if($($('.album'+window.curAlbumID)[i].parentNode)[0].id == url) {
				window.currentAlbumItem = $($('.album'+window.curAlbumID)[i].parentNode).find('.song-number')[0].id.split('song')[1];
			}
			
		}
		// show share and DL button
		document.getElementById('sharer').style.display="table-cell";
			document.getElementById('keeper').style.display="table-cell";
}

function seekHandler(ui) { // jquery ui handler
	if(window.player == undefined) return;
      	if(window.player.buffered < 100) {
	      	$( ".track-progress" ).seekslider('option','value',(window.player.currentTime / window.player.duration)*1000);
	      	return;
      	}
        window.player.seek(Math.round((window.player.duration / 1000) * ui.value));
}

window.isShufflin = false;

window.preShuffleQ = new Array();
function shuffleQ() {
	window.isShufflin = true;
		
		window.preShuffleQ = window.queue.slice(0);
		var nbackup = undefined;
		if(window.curQueueItem > -1) {
			nbackup = window.queue[window.curQueueItem];
			delete window.queue[window.curQueueItem];
		}
		window.queue = arrayShuffle(window.queue);
		window.queue.unshift(nbackup);
		window.queue =  window.queue.filter(function(n){return n});
		window.curQueueItem = 0;
}

function unshuffleQ() {
		window.queue =  window.preShuffleQ.slice(0);
		window.preShuffleQ = new Array();
		window.isShufflin = false;
		window.curQueueItem = window.queue.indexOf(curUrl);
}

function shuffle() {
	if(window.isShufflin) {
		unshuffleQ();
		$('#shu').removeClass('on');
	} else {
		shuffleQ();
		$('#shu').addClass('on');			
	}
	// Shuffling is experimental
}