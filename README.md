# WebTunes
## The Ultimate Music Library Web App
If you are one of those who can't live without music, and you want to have your music everywhere with you — this is the web app for you!
Just install it onto your web server, optionally integrate it with SoundCloud, upload your music, and you're good to go!

## Features
* Plays FLAC, WAV, MP3, AAC, ALAC, SoundCloud — thanks to Aurora.js by AudioCogs
* No download of extra software required — should work in all modern browsers
* Ability to control access to music by adding different user accounts
* Sharing ability — no log in needed to listen a playlist by an invitation link. ([Demo on vladkorotnev.me](http://vladkorotnev.me/webtunes/player.php?list=33))
* High sound quality
* Option to download a track to the local computer, either from the host or from SoundCloud
* Import whole albums by just zipping the album's tracks together and then dropping the album's zip file into the window
* Very simple installer
* Make playlists using drag-and-drop
* Based upon open-source components
* Actively updated

## How to get
Just download this whole repository as a ZIP and upload it onto your server.

## How to install
Make sure your server has:

* PHP 5 or later
* cURL in PHP
* Apache 2
* MySQL
* Modules for MySQL in PHP, and PHP in Apache2 (yes I know someone who tried to install on a server without anything except a bare-bones Apache)

Upload everything to your web server. 
Then chmod the admin directory to 777, or create a file named 'config.php' in it and chmod it 777.
Chmod all other files to 755 so they can be launched.
Chmod folders 'upload', 'useralbums', 'usertracks' to 777.

Go to http://*your server*/*path on server to webtunes*/installer.
The installer will ask you a few questions.

* WebRoot is where WebTunes is located. Should match the SoundCloud API Redirect URL.
* MySQL host, user, server, password are self-descriptive.
* Admin credentials are what a superuser will use to login to the WebTunes system.
* SoundCloud API and Secret are what you get by signing up on SoundCloud Developers. (See below)

*Please note you can't change any of the settings later, unless manually editing config.php*
When you're done filling in the form, click "Next" and the installation will start right away, taking some time to write the config file and the database.

**After finishing the installation, delete the 'installer' folder!**
Failing to do so will create a serious vulnerability on your WebTunes installation.
Now you're ready to upload some music.

## Integrating with SoundCloud
* Go to [SoundCloud Developer Portal](http://soundcloud.com/you/apps)
* Create a new application ID
* As the redirect URI, **specify http://*your server*/*path on server to webtunes*/sc_auth.php**, not just the root directory of WebTunes!
* Save it and copy the API key and the consumer secret into the WebTunes Installer or config.php.

## Adding Music
* Login to WebTunes
* Find the music you want to upload
* If it's an album, ZIP the tracks together, making sure all the tracks are in the root of the archive.
* Drag and drop the file into the WebTunes window
* If it's a single track, it will appear in the "Unsorted music" category. If it's an album, you will be presented with a form to add the album name and artist name, and then it will appear under Albums in the sidebar.
* Enjoy your tunes

*Please note some files may fail with "Demuxer not found". This seems to be a bug in Aurora.js, converting the faulty file to the same format with a known-to-work converter helps fix this*

## Deleting Music
In order to delete a track, drag it to the trash. If you dragged all the album's tracks to trash, the album is considered deleted and is removed from the database.
To delete a playlist, click the trash icon.
To exclude a track from a playlist, drag it to the trash as if you were to delete it.

## Playlist Capabilities
Playlists are playlists, they can contain songs, but, at the moment, only songs on the web host, not from SoundCloud.
If someone sent you a playlist, you can listen to it. If you happen to also have an account, you can copy the playlist for your listening pleasure by clicking the (+) icon near its title.
You can share a playlist by clicking the Share button in the lower right corner of the window.

## SoundCloud Integration
As of 2013/Aug/17, you can:

* Listen to your SoundCloud feed — use the Feed tab
* Search SoundCloud for tracks — use the search bar at the top
* Listen to your own tracks — use the "Own Tracks" tab
* Listen to your SoundCloud favorites — look in the "Favorites" tab
* Listen to your SoundCloud sets — they will appear in the sidebar under the SoundCloud section
* Create, edit and delete SoundCloud sets — drop tracks to a set to add them, drag them from the set to trash to remove them from the set, click trash while viewing the set to delete it, click the + button near SoundCloud section to create a new set
* Add and remove favorites — drag and drop tracks into Favorites, drag tracks from Favorites to trash

It also falls back to the <audio> tag instead of Aurora, so it may or may not work depending on the OS/Browser combinations.
More to come, such as waveforms, likes and comments.
You can download SoundCloud tracks as well, but not yet share them.


## Planned features
* ~~Multiple localization support in progress~~ **See "Translating WebTunes below"**
* Locale switch without reloading page
* Add MOD/XM/S3M/etc. support
* Add MIDI support
* Add multiple skin support (at least colour themes)
* Integration of YouTube (seriously, too much good music there too)
* Integration of VK.com (nice music collection there as well)
* Add visualisations (at least the old iTunes FFT bars in the display)
* Add SunVox file playback support maybe
* Whatever I come up with 
* Suggest your features too

## FAQ

Q: I would like to choose a server for WebTunes, which one should I use?

A: Select the one with enough disk space for your music and from a provider that is OK with music lying on it's disks. A home server is even better.
- - -
Q: My computer exploded when I ran WebTunes!!1

A: This software is provided AS IS, without any warranties, and it's author and contributors can't be held responsible for any damage done.
- - -
Q: Do I have to sign up to SoundCloud in order to use WebTunes without SoundCloud integration?

A: Absolutely not, if you don't fill in the API keys, it will be turned off automatically.


## Contributing

I don't quite keep to some coding style, so as long as your code is easily readable, it's all fine.
When submitting pull requests, explain what have been added, removed or fixed.
When creating issues, explain:

* Expected behaviour
* Real behaviour
* OS / Browser
* Any logs or info that you may have collected
* Possible fix suggestions, if any

## Translating WebTunes
In order to translate WebTunes, you should create a language file that will contain all the strings for WebTunes and be named like `<Language Code>.js>`, for example `ru.js` for Russian, `es.js` for Spanish and so on. Then place it into the 'localization' directory and test it by going to WebTunes with `?forceLocale=<Your Language Code>` (ex. `http://mysite.com/webtunes/player.php?forceLocale=es`).
Please note that all language files should begin with the following format header: ``//LANG:<Lang code>:<Lang name>``, for example the `ru.js` file containing Russian locale begins with `//LANG:ru:Русский`.
In case you need to put a .js file in 'localization', and do NOT want it to even be opened for language search routines, begin it with `//NOTLANG`.


## Documentation
IMO the code is quite readable. Anyway look for 'whatsitdo' folders inside other folders (like in 'js') that has explanation for the parent folder's files.


## Thanks to

* PBXg33k (general improvements)
* yvitalyv (hints and fixes)
* AudioCogs (Aurora.js decoder and player)
* Chris Valesskey (Design)

## About the developer
WebTunes was developed by Vladislav Korotnev in 2013.
Website: [http://vladkorotnev.github.io](http://vladkorotnev.github.io)

