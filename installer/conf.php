<?

	$soft = 'WebTunes Alpha';
	$welcome_tagline = 'Welcome to the '.$soft.' installer! Thanks for choosing software by vladkorotnev :)';
	$beginInstall='Install';
	$submit = 'Next';

	$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
	$sp = strtolower($_SERVER["SERVER_PROTOCOL"]);
    $protocol = substr($sp, 0, strpos($sp, "/")) . $s;
	$directory = explode('/', $_SERVER['REQUEST_URI']);
	$port = ($protocol == "http" && $_SERVER['SERVER_PORT'] == "80" || $protocol == "https" && $_SERVER['SERVER_PORT'] == "443") ? "" : $_SERVER['SERVER_PORT'];
	$webroot =  $protocol . '://' . $_SERVER['HTTP_HOST'] . substr($_SERVER['REQUEST_URI'],0,-strlen( $directory[ count($directory) - 2 ])-1);
	//------------

	$preconf_title = 'Configuration';
	$preconf_processor = 'preconf.php';
	function genInputsPreconf() {
	// make table of inputs
	global $webroot;
		$inputs = ' <tr><td>Public root URL</td><td><input type="text" name="webroot" value="'. $webroot .'"></td></tr>
					<tr><td>MySQL server</td><td><input type="text" name="dbhost" value="127.0.0.1"></td></tr>
					<tr><td>MySQL username</td><td><input type="text" name="dbuser" value="root"></td></tr>
					<tr><td>MySQL password</td><td><input type="password" name="dbpass" value=""></td></tr>
					<tr><td>Admin username</td><td><input type="text" name="admin" value="admin"></td></tr>
					<tr><td>Admin password</td><td><input type="text" name="adminpass" value=""></td></tr>
					<tr><td>SoundCloud APIKey</td><td><input type="text" name="lastapi" value=""></td></tr>
					<tr><td>SoundCloud Secret</td><td><input type="text" name="lastsec" value=""></td></tr>
					';
		return $inputs;
	}
	
	// -------------
	
	$installation_title = 'Installing...';
	$installation_tagline = 'That was easy, wasn\'t it?';
	
	// -------------
	
	$done_title = 'Installation finished!';
	$done_tagline = 'Don\'t forget to delete the installer folder!';
	
?>