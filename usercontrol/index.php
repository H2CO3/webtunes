<? //User control window 
require('../config.php'); 
$perPage=10;
	if(!isset($_SESSION)) {
	session_start();
	}
$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0);
mysql_select_db($dbname, $dlink);
mysql_query ("set character_set_client='utf8'", $dlink); 
mysql_query ("set character_set_results='utf8'", $dlink); 
mysql_query ("set collation_connection='utf8_general_ci'", $dlink); 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>User control</title>
    <meta charset="utf-8">
	<meta name="author" content="Vladislav Korotnev">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->

    <!-- Bootstrap -->
    
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">


  </head>
  <body style="background-color:#eee">
  <?
  	if($_SESSION['uid'] != 'a') {
	  	die('<div class="alert alert-block" style="border-radius:0"><center>Not admin</center><br><center><a href="../player.php" class="btn btn-primary">Player</a>&nbsp;<a href="../logoff.php" class="btn btn-danger">Logoff</a></div>');
  	}
  ?>
 <div id="properContent" class="proper-content">
<a class="btn btn-large" href="../player.php">Exit</a><center><a class="btn btn-large" href="addUser.php"><i class="icon-plus"></i> Add</a></center>
<table style=" background-color:#fff;" class="table table-bordered table-striped table-hover table-shadow">
<thead style="background-color: #eee;">
    <tr>

	<td style="width:10%;">ID</td>
	<td>Login</td>
	<td style="width:10%;">Action</td>
    </tr></thead><tbody>
<?

$sql = "SELECT COUNT(id) FROM logins";
$rs_result = mysql_query($sql,$dlink); 
if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
$row = mysql_fetch_row($rs_result); 
$total_records = $row[0];
$total_pages = ceil($total_records / intval($perPage));
if($total_records == 0){
	echo "<div class=\"alert alert-info alert-block\"><center>Nothin! (Admin doesn't count)</center></div>";
}
// ---- paginator
if ($total_pages > 0) {
	 echo '<center><div class="pagination';

	  echo '"><ul>';
	  
	  if($page == 1) {
		  echo '<li class="disabled"><a href="#"><</a></li>';
	  } else {
		  echo '<li><a href="';
		  echo "usercontrol.php?page=".($page-1);
		  echo '"><</a></li>';
	  }
	
	for ($i=1; $i<=$total_pages; $i++) { 
		if($i==$page) {
			echo '<li class="active"><a href="#">'.$i.'</a></li>';
		} else {
	    echo "<li><a href='usercontrol.php?page=".$i;
	    echo "'>";
	    echo $i."</a></li> "; 
	    }
	}
	if($page == $total_pages) {
		  echo '<li class="disabled"><a href="#">></a></li>';
	  } else {
		  echo '<li><a href="';
		  echo "usercontrol.php?page=".($page+1);
		  echo '">></a></li>';
	  }
	
	echo "</ul></div></center>";
}
$qu="SELECT * FROM logins ";
 $start_from = ($page-1) * intval($perPage);
$qu=$qu."ORDER BY id DESC LIMIT ".$start_from.", ".$perPage;

$result = mysql_query($qu, $dlink);
while($row = mysql_fetch_array($result)){
   	echo '<tr>';
   	
   	echo '<td>';
   	echo $row['id'];
   	echo '</td><td>';
   	echo $row['name'];
   	echo '</td><td>';
   	echo '<a class="btn" href="addUser.php?tgt='.$row['id'].'">Edit</a>&nbsp;<a class="btn btn-danger" href="addUser.php?act=del&tgt='.$row['id'].'">Kill</a>';
   	echo '</td></tr>';
   	
}
mysql_close($dlink);
?>
    </tbody>
</table>
</div>

      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p><small>a vladkorotnev software product.</small></p>
      </footer>

    </div>
       </div>
    <script src="..//jquery.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>