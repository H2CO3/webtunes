//LANG:en:English
// The line above marks the language
function makeStrings() {
	window.locale.appname = 'WebTunes';
	window.locale.vername = 'beta';
	window.locale.credit = '';
	
	// --------- Error and messages
	window.locale.errors.guestUpload = 'Uploading not allowed for guests';
	window.locale.errors.uploadSuccess = 'Upload finished';
	window.locale.errors.uploadFail = 'Upload Failed';
	window.locale.errors.login = 'This system requires either a login or an invitation link to an album. Click OK to log in.';
	window.locale.errors.albumSaved = "Album saved";
	window.locale.errors.linkError = 'Cannot symlink into no album or no category';
	window.locale.errors.saved = 'Saved';
	window.locale.errors.uploading = 'Upload started';
	window.locale.errors.notYourList = 'You can only edit your own playlists';
	window.locale.errors.fail = "Failed";
	window.locale.errors.queueIsUndefined = 'Queue does not exist';
	window.locale.errors.playError = 'Error playing';
	window.locale.errors.deleted = 'Deleted';
	window.locale.errors.DLStart = "Download started";
	window.locale.errors.DLStartSC = "Download starting, please wait.. (SoundCloud downloads are beta)";
	
	// --------- UI strings
	window.locale.ui.uploading = "Uploading...";
	window.locale.ui.wait = 'Wait...';
	window.locale.ui.cloudStream = "Preloading from SoundCloud (may be inaccurate)";
	window.locale.ui.buffering = "Prebuffering...";
	window.locale.ui.bufferingStalled = "Buffering stalled, it seems...";
	window.locale.ui.soundCloudBeta = "SoundCloud integration beta";
	window.locale.ui.onSC = " on SoundCloud";
	window.locale.ui.diskSpace = "Disk Space: ";
	window.locale.ui.artist = 'Artist';
	window.locale.ui.album = 'Album';
	window.locale.ui.compilation = 'Compilation';
	window.locale.ui.songNameUnkn = 'Unknown Song Name';
	window.locale.ui.songBandUnkn = 'Unknown Artist';
	window.locale.ui.songAlbumUnkn= 'Unknown Album';
	window.locale.ui.shareTune = 'Share this track';
	window.locale.ui.keepTune = 'Download this track';
	window.locale.ui.usercontrol = 'edit users';
	window.locale.ui.linkImport = 'symlink-based import';
	window.locale.ui.linkTrack = 'symlink a track';
	window.locale.ui.searchCloud = 'Search on SoundCloud';
	window.locale.ui.cloudFeed = 'Feed';
	window.locale.ui.cloudFavs = 'Favorites';
	window.locale.ui.cloudOwn = 'Uploads';
	window.locale.ui.cloudSearch = 'Search';
	window.locale.ui.about = "About";
	window.locale.ui.shuffle = "Shuffle";
	window.locale.ui.logon = "Login";
	window.locale.ui.logoff= "Logoff";
	window.locale.ui.rename = "Rename";
	window.locale.ui.share = "Share";

	// --------- Dialog strings
	window.locale.dialogs.albumSave = "Save this album";
	window.locale.dialogs.albumDiscard = "Discard this album";
	window.locale.dialogs.linkTrack = "Save this track";
	window.locale.dialogs.cancel = "Cancel";
	window.locale.dialogs.plsRename = 'Input name for this playlist';
	window.locale.dialogs.copyLink = 'Copy this link to share this playlist. The receiving user will be able to listen to it and copy it to his collection.';
	window.locale.dialogs.copyLinkTrk = 'Copy this link to share the current track. The user will be able to see the whole album/playlist that contains the track.';
	window.locale.dialogs.trash = 'Drag songs here to delete them';
	
	// --- Multipart String 'Really delete playlist NAME? To delete....'
	window.locale.dialogs.del1 = 'Really delete playlist ';
	window.locale.dialogs.del2 = '? To delete songs from it, drag them onto the trash button';
	// --- End multipart string
	
	window.locale.dialogs.newList = 'New playlist name?';
	window.locale.dialogs.unamedList = 'Untitled playlist';
	
	window.locale.dialogs.newSClist = 'New SoundCloud playlist name?';
	window.locale.dialogs.unamedSClist = 'WebTunes playlist';
	
	
	// --------- Sidebar strings
	window.locale.sidebar.library = "Library";
	window.locale.sidebar.music = "Music";
	window.locale.sidebar.compilations = "Compilations";
	window.locale.sidebar.unsorted = "Unsorted music";
	window.locale.sidebar.playlists = "Playlists";
	window.locale.sidebar.soundcloud = 'SoundCloud';
	window.locale.sidebar.albums = 'Albums';
}
