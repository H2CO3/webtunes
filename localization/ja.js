//LANG:ja:日本語
// The line above marks the language
function makeStrings() {
	window.locale.appname = 'WebTunes';
	window.locale.vername = 'beta';
	window.locale.credit = 'WebTunes Teamとichitasoによる日本語訳';
	
	// --------- Error and messages
	window.locale.errors.guestUpload = 'アップロードにはログインが必要です';
	window.locale.errors.uploadSuccess = 'アップロードが終了しました';
	window.locale.errors.uploadFail = 'アップロードエラー';
	window.locale.errors.login = 'WebTunesにログインために「OK」をクリックしてください';
	window.locale.errors.albumSaved = "アルバムを保存しました";
	window.locale.errors.linkError = 'アルバムやカテゴリへのリンクを作ることのみ可能です';
	window.locale.errors.saved = '保存しました';
	window.locale.errors.uploading = 'アップロードが開始されました';
	window.locale.errors.notYourList = '自分のプレイリストのみ編集することができます';
	window.locale.errors.fail = "エラー";
	window.locale.errors.queueIsUndefined = 'システムエラー：　キューが存在しません';
	window.locale.errors.playError = '再生エラー：';
	window.locale.errors.deleted = '削除しました';
	window.locale.errors.DLStart = "ダウンロードが開始されました";
	window.locale.errors.DLStartSC = "ダウンロード中です・・・（SoundCloudダウンロードはベータです）";
	
	// --------- UI strings
	window.locale.ui.uploading = "アップロード中です・・・";
	window.locale.ui.wait = 'お待ちください・・・';
	window.locale.ui.cloudStream = "SoundCloudからストリーミング（不正確になることがあります）";
	window.locale.ui.buffering = "バッファリング中・・・";
	window.locale.ui.bufferingStalled = "バッファリングが停止中・・・";
	window.locale.ui.soundCloudBeta = "SoundCloudをベータテスト統合";
	window.locale.ui.onSC = " のSoundCloud";//SoundCloud上で
	window.locale.ui.diskSpace = "空きディスク容量: ";
	window.locale.ui.artist = 'アーティスト';
	window.locale.ui.album = 'アルバム';
	window.locale.ui.compilation = '編集';
	window.locale.ui.songNameUnkn = '不明な音楽';
	window.locale.ui.songBandUnkn = '不明なアーティスト';
	window.locale.ui.songAlbumUnkn= '不明なアルバム';
	window.locale.ui.shareTune = 'トラックをシェア';
	window.locale.ui.keepTune = 'トラックをダウンロード';
	window.locale.ui.usercontrol = 'ユーザを編集';
	window.locale.ui.linkImport = 'リンクをインポート';
	window.locale.ui.linkTrack = 'トラックのリンクを作る';
	window.locale.ui.searchCloud = 'SoundCloudで検索';
	window.locale.ui.cloudFeed = 'フィード';
	window.locale.ui.cloudFavs = 'お気に入り';
	window.locale.ui.cloudOwn = 'アップロード';
	window.locale.ui.cloudSearch = '検索';
	window.locale.ui.about = "About";
	window.locale.ui.shuffle = "シャッフル";
	window.locale.ui.logon = "ログイン";
	window.locale.ui.logoff= "ログオフ";
	window.locale.ui.rename = "リネーム";
	window.locale.ui.share = "送信";

	// --------- Dialog strings
	window.locale.dialogs.albumSave = "アルバムを保存する";
	window.locale.dialogs.albumDiscard = "アルバムを削除する";
	window.locale.dialogs.linkTrack = "トラックを保存する";
	window.locale.dialogs.cancel = "キャンセル";
	window.locale.dialogs.plsRename = 'プレイリストの名前を入力してください';
	window.locale.dialogs.copyLink = 'プレイリストを送信するために、そのリンクをコピーしてください。受信者が聞くことができるようになります。プレイリストをコピーすることもできるようになります。';
	window.locale.dialogs.copyLinkTrk = 'トラックを送信するために、そのリンクをコピーしてください。受信者は、そのプレイリスト/アルバムを聞くことができるようになります。その音楽は自動的に起動します。';
	window.locale.dialogs.trash = 'ファイルを削除するために、このアイコンにファイルをドラッグしてください';
	
	// --- Multipart String 'Really delete playlist NAME? To delete....'
	window.locale.dialogs.del1 = '本当に「';
	window.locale.dialogs.del2 = '」のプレイリストを削除しますか？曲を一つずつ除外するには、プレイリストからゴミ箱にファイルをドラッグしてください。';
	// --- End multipart string
	
	window.locale.dialogs.newList = '新しいプレイリストの名前を入力してください。';
	window.locale.dialogs.unamedList = '不明のプレイリスト';
	
	window.locale.dialogs.newSClist = '新しいSoundCloudプレイリストの名前を入力してください';
	window.locale.dialogs.unamedSClist = '不明のSoundCloudプレイリスト';
	
	
	// --------- Sidebar strings
	window.locale.sidebar.library = "ライブラリ";
	window.locale.sidebar.music = "音楽";
	window.locale.sidebar.compilations = "コンピレーション";
	window.locale.sidebar.unsorted = "ソートされていない曲";
	window.locale.sidebar.playlists = "プレイリスト";
	window.locale.sidebar.soundcloud = 'SoundCloud';
	window.locale.sidebar.albums = 'アルバム';
}