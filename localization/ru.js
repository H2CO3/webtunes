//LANG:ru:Русский
// The line above marks the language
function makeStrings() {
	window.locale.appname = 'WebTunes';
	window.locale.vername = 'beta';
	window.locale.credit = 'Русский перевод — команда WebTunes';
	
	// --------- Error and messages
	window.locale.errors.guestUpload = 'Загрузка файлов без входа в систему запрещена';
	window.locale.errors.uploadSuccess = 'Загрузка завершена';
	window.locale.errors.uploadFail = 'Ошибка загрузки';
	window.locale.errors.login = 'Требуется войти в систему. Нажмите ОК для перехода на страницу логина.';
	window.locale.errors.albumSaved = "Альбом сохранен";
	window.locale.errors.linkError = 'Нельзя делать ссылку куда либо кроме альбома или категории';
	window.locale.errors.saved = 'Сохранено';
	window.locale.errors.uploading = 'Загрузка начата';
	window.locale.errors.notYourList = 'Нельзя редактировать чужие плейлисты';
	window.locale.errors.fail = "Ошибка";
	window.locale.errors.queueIsUndefined = 'Очередь не существует';
	window.locale.errors.playError = 'Ошибка воспроизведения';
	window.locale.errors.deleted = 'Удалено';
	window.locale.errors.DLStart = "Скачивание запущено";
	window.locale.errors.DLStartSC = "Скачивание запускается.. (скачивание с SoundCloud пока что экспериментальное)";
	
	// --------- UI strings
	window.locale.ui.uploading = "Загрузка...";
	window.locale.ui.wait = 'Подождите...';
	window.locale.ui.cloudStream = "Подгрузка с SoundCloud (может быть неточной)";
	window.locale.ui.buffering = "Подгрузка...";
	window.locale.ui.bufferingStalled = "Похоже, подгрузка зависла...";
	window.locale.ui.soundCloudBeta = "Экспериментальная интеграция SoundCloud";
	window.locale.ui.onSC = " на SoundCloud";
	window.locale.ui.diskSpace = "Свободное место: ";
	window.locale.ui.artist = 'Исполнитель';
	window.locale.ui.album = 'Альбом';
	window.locale.ui.compilation = 'Это сборник';
	window.locale.ui.songNameUnkn = 'Неименованная песня';
	window.locale.ui.songBandUnkn = 'Неименованный исполнитель';
	window.locale.ui.songAlbumUnkn= 'Неименованный альбом';
	window.locale.ui.shareTune = 'Поделиться треком';
	window.locale.ui.keepTune = 'Скачать трек';
	window.locale.ui.usercontrol = 'редактор пользователей';
	window.locale.ui.linkImport = 'импорт симлинком';
	window.locale.ui.linkTrack = 'симлинк трека';
	window.locale.ui.searchCloud = 'Поиск по SoundCloud';
	window.locale.ui.cloudFeed = 'Новые';
	window.locale.ui.cloudFavs = 'Избранное';
	window.locale.ui.cloudOwn = 'Загруженное';
	window.locale.ui.cloudSearch = 'Поиск';
	window.locale.ui.about = "Инфо";
	window.locale.ui.shuffle = "Перемешать";
	window.locale.ui.logon = "Войти";
	window.locale.ui.logoff= "Выход";
	window.locale.ui.rename = "Переименовать";
	window.locale.ui.share = "Отправить";

	// --------- Dialog strings
	window.locale.dialogs.albumSave = "Сохранить альбом";
	window.locale.dialogs.albumDiscard = "Удалить альбом";
	window.locale.dialogs.linkTrack = "Сохранить трек";
	window.locale.dialogs.cancel = "Отмена";
	window.locale.dialogs.plsRename = 'Введите имя для этого плейлиста';
	window.locale.dialogs.copyLink = 'Скопируйте ссылку, чтобы поделиться плейлистом. Получатель сможет его слушать, и, при наличии аккаунта, скопировать его к себе.';
	window.locale.dialogs.copyLinkTrk = 'Скопируйте ссылку, чтобы поделиться этим треком. Получатель также получит доступ к содержащему его плейлисту/альбому.';
	window.locale.dialogs.trash = 'Это корзина. Перетаскивайте треки сюда, чтобы удалять их.';
	
	// --- Multipart String 'Really delete playlist NAME? To delete....'
	window.locale.dialogs.del1 = 'Вы действительно хотите удалить плейлист «';
	window.locale.dialogs.del2 = '»? Чтобы исключать из него песни, просто перетаскивайте их в корзину.';
	// --- End multipart string
	
	window.locale.dialogs.newList = 'Введите название нового плейлиста';
	window.locale.dialogs.unamedList = 'Моя любимая музыка';
	
	window.locale.dialogs.newSClist = 'Введите название нового сета на SoundCloud';
	window.locale.dialogs.unamedSClist = 'Плейлист из '+window.locale.appname;
	
	
	// --------- Sidebar strings
	window.locale.sidebar.library = "Библиотека";
	window.locale.sidebar.music = "Музыка";
	window.locale.sidebar.compilations = "Сборники";
	window.locale.sidebar.unsorted = "Несортированное";
	window.locale.sidebar.playlists = "Плейлисты";
	window.locale.sidebar.soundcloud = 'SoundCloud';
	window.locale.sidebar.albums = 'Альбомы';
}
