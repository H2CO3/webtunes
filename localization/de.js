//LANG:de:Deutsch
// The line above marks the language
function makeStrings() {
	window.locale.appname = 'WebTunes';
        window.locale.vername = 'beta';
        window.locale.credit = 'deutsche Übersetzung von @isbf97';
        
        // --------- Error and messages
        window.locale.errors.guestUpload = 'Das Hochladen ist nicht Erlaubt für Gäste';
        window.locale.errors.uploadSuccess = 'Hochladen fertig';
        window.locale.errors.uploadFail = 'Hochladen fertiggestellt';
        window.locale.errors.login = 'Dieses System benötigt eine Anmeldung oder eine Einladung zu einem Album. Klicken sie Ok um dich anzumelden.';
        window.locale.errors.albumSaved = "Album gespeichert";
        window.locale.errors.linkError = 'Cannot symlink into no album or no category';       //  (DON'T KNOW HOW TO TRANSLATE THIS LINE... PLEASE ASK SOMEONE ELSE) sorry :D
        window.locale.errors.saved = 'gespeichert';
        window.locale.errors.uploading = 'Hochladen gestartet';
        window.locale.errors.notYourList = 'Du kannst deine Playlist nicht bearbeiten';
        window.locale.errors.fail = "Fehlgeschlagen";
        window.locale.errors.queueIsUndefined = 'Queue existiert nicht';         //  (Queue in german = ???)
        window.locale.errors.playError = 'Fehler beim Abspielen';
        window.locale.errors.deleted = 'gelöscht';
        window.locale.errors.DLStart = "Download gestartet";
        window.locale.errors.DLStartSC = "Download startet, bitte warten.. (SoundCloud downloads sind beta)";
        
        // --------- UI strings
        window.locale.ui.uploading = "Hochladen...";
        window.locale.ui.wait = 'Warten...';
        window.locale.ui.cloudStream = "Preloading von der SoundCloud (könnte ungenau sein)";
        window.locale.ui.buffering = "Prebuffering...";             //(I also don't know how to translate...:( )
        window.locale.ui.bufferingStalled = "Buffering stalled, it seems...";   //(same)    
        window.locale.ui.soundCloudBeta = "SoundCloud integration beta";
        window.locale.ui.onSC = " in der SoundCloud";
        window.locale.ui.diskSpace = "Festplatten spe.: ";
        window.locale.ui.artist = 'Künstler';
        window.locale.ui.album = 'Album';
        window.locale.ui.compilation = 'Zusammenstellung';
        window.locale.ui.songNameUnkn = 'Unbekannter Liedername';
        window.locale.ui.songBandUnkn = 'Unbekannter Künstler';
        window.locale.ui.songAlbumUnkn= 'Unbekanntes ALbum';
        window.locale.ui.shareTune = 'Teile das Lied';
        window.locale.ui.keepTune = 'Lade das Lied runter';
        window.locale.ui.usercontrol = 'bearbeite Nutzer';
        window.locale.ui.linkImport = 'symlink-based import';       //(damn... don't know in german)
        window.locale.ui.linkTrack = 'symlink a track';           //  (same again)
        window.locale.ui.searchCloud = 'Suche auf der Soundcloud';
        window.locale.ui.cloudFeed = 'Feed';     // (?)
        window.locale.ui.cloudFavs = 'Favoriten';
        window.locale.ui.cloudOwn = 'Hochgeladende Dateien';
        window.locale.ui.cloudSearch = 'Suchen';
        window.locale.ui.about = "Über";
        window.locale.ui.shuffle = "Shuffle";     //(?...)
        window.locale.ui.logon = "Einloggen";
        window.locale.ui.logoff= "Ausloggen";
        window.locale.ui.rename = "Umbenennen";
        window.locale.ui.share = "Teilem";

        // --------- Dialog strings
        window.locale.dialogs.albumSave = "Speicher das Album";
        window.locale.dialogs.albumDiscard = "Album verwerfen";
        window.locale.dialogs.linkTrack = "Speicher das Lied";
        window.locale.dialogs.cancel = "Abbrechen";
        window.locale.dialogs.plsRename = 'Benenne die Playlist';
        window.locale.dialogs.copyLink = 'Kopiere den Link, um die Playlist zu teilen. Der empfangende Nutzer wird die Playlist hören können und kann sie zu seiner Sammlung kopieren.';
        window.locale.dialogs.copyLinkTrk = 'Kopiere den Link um das aktuelle Lied zu teilen. Der Nutzer wird das ganze Album sehen können und die Playlisten die das Lied enthalten.';
        window.locale.dialogs.trash = 'Ziehe die Lieder hier hin, um die zu löschen';
        
        // --- Multipart String 'Really delete playlist NAME? To delete....'
        window.locale.dialogs.del1 = 'Really delete playlist ';       // (also don't know how to translate)
        window.locale.dialogs.del2 = '? Um Lieder von ihr zu löschen, zieh sie auf den Löschbutton';
        // --- End multipart string
        
        window.locale.dialogs.newList = 'Neuer Playlistname?';
        window.locale.dialogs.unamedList = 'Unbenannte Playlist';
        
        window.locale.dialogs.newSClist = 'Neuer SoundCloud Playlistname?';
        window.locale.dialogs.unamedSClist = 'WebTunes Playlist';
        
        
        // --------- Sidebar strings
        window.locale.sidebar.library = "Bibliotek";
        window.locale.sidebar.music = "Musik";
        window.locale.sidebar.compilations = "Zusammenstellungen";
        window.locale.sidebar.unsorted = "Unsortierte Musik";
        window.locale.sidebar.playlists = "Playlisten";
        window.locale.sidebar.soundcloud = 'SoundCloud';
        window.locale.sidebar.albums = 'Alben';
}
