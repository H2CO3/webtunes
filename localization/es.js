//LANG:es:Español
function makeStrings () {
        window.locale.appname = 'WebTunes';
        window.locale.vername = 'beta';
        window.locale.credit = "traducción en Español (Castellano) por Selesn777";
        
        // --------- Mensajes de error y
        window.locale.errors.guestUpload = 'Subir archivos sin registro desactivado';
        window.locale.errors.uploadSuccess = 'Subir completo ';
        window.locale.errors.uploadFail = 'No se ha podido cargar la ruta ';
        window.locale.errors.login = 'Se necesita iniciar sesión. Haga clic en Aceptar para ir a la página de inicio de sesión ';
        window.locale.errors.albumSaved = "Álbum guardado";
        window.locale.errors.linkError = 'No se puede hacer mucho más que un enlace a un álbum o la categoría';
        window.locale.errors.saved = 'Guardado';
        window.locale.errors.uploading = 'carga comenzó';
        window.locale.errors.notYourList = 'No puede editar otras listas de reproducción';
        window.locale.errors.fail = "Error";
        window.locale.errors.queueIsUndefined = 'cola no existe';
        window.locale.errors.playError = 'error de reproducción';
        window.locale.errors.deleted = 'Borrado';
        window.locale.errors.DLStart = "Descargar iniciado";
        window.locale.errors.DLStartSC = "comienza .. Descarga (descargar en SoundCloud todavía experimental)";
        
        // --------- Cadenas de la IU
        window.locale.ui.uploading = "Cargando...";
        window.locale.ui.wait = "Por favor espere...";
        window.locale.ui.cloudStream = "subir a SoundCloud (puede ser inexacta)";
        window.locale.ui.buffering = "uploading...";
        window.locale.ui.bufferingStalled = "Parece que la carga de colgado ...";
        window.locale.ui.soundCloudBeta = "integración experimental de SoundCloud";
        window.locale.ui.onSC = "en SoundCloud";
        window.locale.ui.diskSpace = "Espacio libre:";
        window.locale.ui.artist = 'artista';
        window.locale.ui.album = 'Album';
        window.locale.ui.compilation = 'Esta es una colección';
        window.locale.ui.songNameUnkn = 'canción sin nombre';
        window.locale.ui.songBandUnkn = 'artista sin nombre';
        window.locale.ui.songAlbumUnkn = 'disco sin nombre';
        window.locale.ui.shareTune = 'Compartir pista';
        window.locale.ui.keepTune = 'Descarga de la pista';
        window.locale.ui.usercontrol = 'editor en los usuarios';
        window.locale.ui.linkImport = 'import enlace simbólico';
        window.locale.ui.linkTrack = 'pista enlace simbólico';
        window.locale.ui.searchCloud = 'Buscar SoundCloud';
        window.locale.ui.cloudFeed = 'nuevo';
        window.locale.ui.cloudFavs = 'Favoritos';
        window.locale.ui.cloudOwn = 'La subida';
        window.locale.ui.cloudSearch = 'Buscar';
        window.locale.ui.about = "info";
        window.locale.ui.shuffle = "Shuffle";
        window.locale.ui.logon = "Login";
        window.locale.ui.logoff = "Salir";
        window.locale.ui.rename = "Cambiar nombre";
        window.locale.ui.share = "Enviar";

        window.locale.dialogs.albumSave = "Guardar Album";
        window.locale.dialogs.albumDiscard = "Delete Album";
        window.locale.dialogs.linkTrack = "Guardar pista";
        window.locale.dialogs.cancel = "Cancelar";
        window.locale.dialogs.plsRename = 'Escriba un nombre para la lista de reproducción ';
         window.locale.dialogs.copyLink = 'Copiar enlace para compartir la lista. El destinatario podrá escuchar a él, y, si está registrada, copia a ti mismo ';
         window.locale.dialogs.copyLinkTrk = 'Copiar enlace para compartir esta canción. El destinatario también tendrá acceso a la lista de reproducción que contenga / álbum ';
        window.locale.dialogs.trash = 'Esta es la canasta. Arrastre las canciones aquí para eliminarlas';
        
        
        window.locale.dialogs.del1 = '¿Está seguro que desea eliminar una lista de reproducción ';
        window.locale.dialogs.del2 = '»? Para excluirlo de la canción, simplemente arrástrelos a la Papelera ';
     
        
        window.locale.dialogs.newList = 'Introduzca el nombre de la nueva lista de reproducción';
        window.locale.dialogs.unamedList = "¿Qué tipo de música ";
        
        window.locale.dialogs.newSClist = 'Introduzca el nombre del nuevo grupo en SoundCloud';
        window.locale.dialogs.unamedSClist = 'Lista de reproducción de '+ window.locale.appname;
        

        window.locale.sidebar.library = "biblioteca";
        window.locale.sidebar.music = "Music";
        window.locale.sidebar.compilations = "Colecciones";
        window.locale.sidebar.unsorted = "ordenados";
        window.locale.sidebar.playlists = "Listas de reproducción";
        window.locale.sidebar.soundcloud = 'SoundCloud';
        window.locale.sidebar.albums = 'Album';
 }