//NOTLANG
// The above string designates the file is not a localization file
function prepareLocale() {
	window.locale = new Object();
	window.locale.ui = new Object();
	window.locale.dialogs = new Object();	
	window.locale.sidebar = new Object;
	window.locale.errors = new Object();
	makeStrings();
	
	
	// ---- Write some stuff into HTML
	locElementByToken('apptitle', 'APPNAME', window.locale.appname + ' ' + window.locale.vername);
	document.title = window.locale.appname;
	locElement('loc_art_lbl', window.locale.ui.artist);
	locElement('loc_alb_lbl', window.locale.ui.album);
	locElement('loc_comp_lbl', window.locale.ui.compilation);
	locElementTitle ('loc_sharer', window.locale.ui.shareTune);
	locElement('song-title', window.locale.ui.songNameUnkn);
	locElement('song-album', window.locale.ui.songAlbumUnkn);
	locElement('song-band', window.locale.ui.songBandUnkn);
	locElementTitle ('loc_keeper', window.locale.ui.keepTune);
	try {
		locElementByToken('userctl', 'USEREDIT', window.locale.ui.usercontrol);
		locElementByToken('symimport', 'SYMIMPORT', window.locale.ui.linkImport);
		locElementByToken('symtrack', 'SYMTRACK', window.locale.ui.linkTrack);
		locElementByToken('disk', 'DSPACE', window.locale.ui.diskSpace);
	} catch(e) { }
		
	try {
		$('#scsearchfield')[0].placeholder = window.locale.ui.searchCloud;
	} catch(e) { }
	
	locElement("loc_about", window.locale.ui.about);
	locElement('loc_shuffle', window.locale.ui.shuffle);
	try {
		locElement('loc_logoff', window.locale.ui.logoff);
	} catch(e) { }
	try {
		locElement('loc_logon', window.locale.ui.logon);
	} catch(e) { }
	locElement('renamelist', window.locale.ui.rename);
	locElement('sharelist', window.locale.ui.share);
}

function locElement(element, text) {
	$('#'+element)[0].innerHTML=text;
}
function locElementByToken(element, token, text) {
	$('#'+element)[0].innerHTML = $('#'+element)[0].innerHTML.replace('__'+token+'__', text);
}
function locElementTitle (element, title) {
	$('#'+element)[0].title=title;
}
function localizeSidebar() {
	try {
		locElementByToken('sidebar', 'MUSIC', window.locale.sidebar.music);
		locElementByToken('sidebar', 'LIBRARY', window.locale.sidebar.library);
		locElementByToken('sidebar', 'COMPILATIONS', window.locale.sidebar.compilations);
		locElementByToken('sidebar', 'UNSORTED', window.locale.sidebar.unsorted);
		locElementByToken('sidebar', 'PLAYLISTS', window.locale.sidebar.playlists);
		
		locElementByToken('sidebar', 'ALBUMS', window.locale.sidebar.albums);
		
	}catch(e) {
		
	}
	
	try {
		locElementByToken('sidebar', 'SOUNDCLOUD', window.locale.sidebar.soundcloud);
		locElementByToken('sidebar', 'SC_FEED', window.locale.ui.cloudFeed);
		locElementByToken('sidebar', 'SC_OWN', window.locale.ui.cloudOwn);
		locElementByToken('sidebar', 'SC_FAVS', window.locale.ui.cloudFavs);
		locElementByToken('sidebar', 'SC_SEARCH', window.locale.ui.cloudSearch);
	} catch(e) 
	{
		
	}
}
