//LANG:fr:Français
// The line above marks the language
function makeStrings() {
	window.locale.appname = 'WebTunes';
	window.locale.vername = 'beta';
	window.locale.credit = 'Les poneys domineront le monde aidés par Taiki';
	
	// --------- Error and messages
	window.locale.errors = new Object();
	window.locale.errors.guestUpload = 'L\'upload n\'est pas autorisé aux invités';
	window.locale.errors.uploadSuccess = 'Upload terminé';
	window.locale.errors.uploadFail = 'Échec de l\'upload';
	window.locale.errors.login = 'Vous avez besoin d\'un compte ou du lien d\'invitation à un album pour utiliser le service. Cliquez sur OK pour vous connecter.';
	window.locale.errors.albumSaved = "Album sauvegardé";
	window.locale.errors.linkError = 'Vous ne pouvez pas créer de raccourcis dans le vide duh.';
	window.locale.errors.saved = 'Sauvegardé!';
	window.locale.errors.uploading = 'Upload en cours';
	window.locale.errors.notYourList = 'Vous ne pouvez éditer la playlist de quelqu\'un d\'autre';
	window.locale.errors.fail = "Échec";
	window.locale.errors.queueIsUndefined = 'Cette file n\'existe pas';
	window.locale.errors.playError = 'Erreur dans la lecture';
	window.locale.errors.deleted = 'Supprimé';
	window.locale.errors.DLStart = "Téléchargement en cours";
	window.locale.errors.DLStartSC = "Démarrage du téléchargement, veuillez patienter... (Cette fonctionnalité de SoundCloud est en béta)";
	
	// --------- UI strings
	window.locale.ui.uploading = "Upload en cours...";
	window.locale.ui.wait = 'Veuillez patienter...';
	window.locale.ui.cloudStream = "Pré-chargement de SoundCloud...";
	window.locale.ui.buffering = "Mise en cache...";
	window.locale.ui.bufferingStalled = "On dirait que la mise en cache est bloquée...";
	window.locale.ui.soundCloudBeta = "Integration SoundCloud en beta";
	window.locale.ui.onSC = " sur SoundCloud";
	window.locale.ui.diskSpace = "Espace dispo.: ";
	window.locale.ui.artist = 'Artiste';
	window.locale.ui.album = 'Album';
	window.locale.ui.compilation = 'Compilation';
	window.locale.ui.songNameUnkn = 'Nom de la chanson inconnus';
	window.locale.ui.songBandUnkn = 'Artiste inconnu';
	window.locale.ui.songAlbumUnkn= 'Album inconnu';
	window.locale.ui.shareTune = 'Partagez cette piste';
	window.locale.ui.keepTune = 'Téléchargez cette piste';
	window.locale.ui.usercontrol = 'modifier l\'utilisateur';
	window.locale.ui.linkImport = 'Importée par un lien';
	window.locale.ui.linkTrack = 'Partagez une piste';
	window.locale.ui.searchCloud = 'Chercher dans SoundCloud';
	window.locale.ui.cloudFeed = 'Flux';
	window.locale.ui.cloudFavs = 'Favoris';
	window.locale.ui.cloudOwn = 'Uploadées';
	window.locale.ui.cloudSearch = 'Rechercher';
	window.locale.ui.about = "About";
	window.locale.ui.shuffle = "Aléatoire";
	window.locale.ui.logon = "Connexion";
	window.locale.ui.logoff= "Déco";
	window.locale.ui.rename = "Renommer";
	window.locale.ui.share = "Partager";

	// --------- Dialog strings
	window.locale.dialogs.albumSave = "Sauver cet album";
	window.locale.dialogs.albumDiscard = "Effacer cet album";
	window.locale.dialogs.linkTrack = "Sauver cette piste";
	window.locale.dialogs.cancel = "Annuler";
	window.locale.dialogs.plsRename = 'Entrez le nom de cette playlist';
	window.locale.dialogs.copyLink = 'Copiez ce lien pour partager cette playlist. Avec, il sera possible de l\'écouter et de la copier dans sa collection.';
	window.locale.dialogs.copyLinkTrk = 'Copiez ce lien pout partager cette piste. Avec, il sera possible de voir l\'intégralité de la playlist/album auquel appartient cette piste.';
	window.locale.dialogs.trash = 'Glissez des chansons ici pour les supprimer';
	
	// --- Multipart String 'Really delete playlist NAME? To delete....'
	window.locale.dialogs.del1 = 'Voulez vous VRAIMENT supprimer la playlist ';
	window.locale.dialogs.del2 = '? Pour en supprimer seulement des éléments, glissez les sur l\'icône en forme de poubelle';
	// --- End multipart string
	
	window.locale.dialogs.newList = 'Comment s\'appelle cette playlist?';
	window.locale.dialogs.unamedList = 'Playlist sans nom';
	
	window.locale.dialogs.newSClist = 'Comment s\'appelle cette playlist SoundCloud?';
	window.locale.dialogs.unamedSClist = 'Playlist WebTunes';
	
	
	// --------- Sidebar strings
	window.locale.sidebar.library = "Librarie";
	window.locale.sidebar.music = "Musique";
	window.locale.sidebar.compilations = "Compilations";
	window.locale.sidebar.unsorted = "Musique non-classée";
	window.locale.sidebar.playlists = "Playlists";
	window.locale.sidebar.soundcloud = 'SoundCloud';
	window.locale.sidebar.albums = 'Albums';
}