function makeStrings() {
        window.locale.appname = 'WebTunes';
        window.locale.vername = 'béta';
        window.locale.credit = 'Magyar fordítás: (C) Goretity Árpád <http://twitter.com/H2CO3_iOS>';
        
        // --------- Hibák és üzenetek
        window.locale.errors.guestUpload = 'Vendégeknek nem engedélyezett a feltöltés';
        window.locale.errors.uploadSuccess = 'A feltöltés kész';
        window.locale.errors.uploadFail = 'Hiba a feltöltés során';
        window.locale.errors.login = 'Ehhez a rendszerhez regisztráció vagy egy album megtekintésére meghívó link szükséges. Kattints az "OK" gombra a bejelentlezéshez.';
        window.locale.errors.albumSaved = "Album elmentve";
        window.locale.errors.linkError = 'Nem lehet létrehozni a symlinket: előbb adj meg egy albumot vagy egy kategóriát!';
        window.locale.errors.saved = 'Elmentve';
        window.locale.errors.uploading = 'A feltöltés elkezdődött';
        window.locale.errors.notYourList = 'Csak a saját lejátszási listáidat szerkesztheted';
        window.locale.errors.fail = "Hiba";
        window.locale.errors.queueIsUndefined = 'A letöltési sor nem létezik';
        window.locale.errors.playError = 'Hiba lejátszás közben';
        window.locale.errors.deleted = 'Törölve';
        window.locale.errors.DLStart = "A letöltés elkezdődött";
        window.locale.errors.DLStartSC = "Letöltés folyamatban, kérlek várj... (a SoundCloud-letöltések bétaverziósak)";
        
        // --------- A felhasználói felület szövegei
        window.locale.ui.uploading = "Feltöltés...";
        window.locale.ui.wait = 'Várj...';
        window.locale.ui.cloudStream = "Betöltés SoundCloud-ról (pontatlanságok előfordulhatnak)";
        window.locale.ui.buffering = "Bufferelés...";
        window.locale.ui.bufferingStalled = "Úgy tűnik, a bufferelés megszakadt.";
        window.locale.ui.soundCloudBeta = "SoundCloud-integráció (béta)";
        window.locale.ui.onSC = " a SoundCloud-on";
        window.locale.ui.diskSpace = "Szabad lemezterület: ";
        window.locale.ui.artist = 'Előadó';
        window.locale.ui.album = 'Album';
        window.locale.ui.compilation = 'Összeállítás';
        window.locale.ui.songNameUnkn = 'Ismeretlen cím';
        window.locale.ui.songBandUnkn = 'Ismeretlen előadó';
        window.locale.ui.songAlbumUnkn= 'Ismeretlen album';
        window.locale.ui.shareTune = 'Szám megosztása';
        window.locale.ui.keepTune = 'Szám letöltése';
        window.locale.ui.usercontrol = 'Felhasználók szerkesztése';
        window.locale.ui.linkImport = 'Symlink-alapú importálás';
        window.locale.ui.linkTrack = 'Szám symlinkelése';
        window.locale.ui.searchCloud = 'Keresés a SoundCloud-ban';
        window.locale.ui.cloudFeed = 'Hírfolyam';
        window.locale.ui.cloudFavs = 'Kedvencek';
        window.locale.ui.cloudOwn = 'Feltöltések';
        window.locale.ui.cloudSearch = 'Keresés';
        window.locale.ui.about = "Névjegy";
        window.locale.ui.shuffle = "Keverés";
        window.locale.ui.logon = "Bejelentkezés";
        window.locale.ui.logoff= "Kilépés";
        window.locale.ui.rename = "Átnevezés";
        window.locale.ui.share = "Megosztás";

        // --------- Dialógusablakok
        window.locale.dialogs.albumSave = "Album mentése";
        window.locale.dialogs.albumDiscard = "Album elvetése";
        window.locale.dialogs.linkTrack = "Szám mentése";
        window.locale.dialogs.cancel = "Mégsem";
        window.locale.dialogs.plsRename = 'Add meg a lejátszási lista nevét';
        window.locale.dialogs.copyLink = 'Másold ki ezt a linket a lejátszási lista megosztásához. A felhasználók meg tudják majd hallgatni és hozzá tudják adni a saját gyűjteményükhöz.';
        window.locale.dialogs.copyLinkTrk = 'Másold ki ezt a linket az éppen játszott szám megosztásához. A felhasználók látni fogják a számot tartalmazó teljes albumot vagy lejátszási listát.';
        window.locale.dialogs.trash = 'Húzd ide a számokat a törléshez';
        
        // --- Többrészes (formázott) string 'Biztosan törlöd a(z) NÉV IDE lejátszási listát? ...'
        window.locale.dialogs.del1 = 'Biztosan törlöd a(z) ';
        window.locale.dialogs.del2 = ' lejátszási listát? Ha csak egyes számokat szeretnél törölni, húzd őket a kukába.';
        // --- Többrészes string vége
        
        window.locale.dialogs.newList = 'Add meg a lista nevét';
        window.locale.dialogs.unamedList = 'Cím nélkül';
        
        window.locale.dialogs.newSClist = 'Add meg a SoundCloud lejátszási lista nevét';
        window.locale.dialogs.unamedSClist = 'WebTunes lejátszási lista';
        
        
        // --------- A "Sidebar" szövegei
        window.locale.sidebar.library = "Könyvtár";
        window.locale.sidebar.music = "Zene";
        window.locale.sidebar.compilations = "Összeállítások";
        window.locale.sidebar.unsorted = "Nem rendezett zene";
        window.locale.sidebar.playlists = "Lejátszási listák";
        window.locale.sidebar.soundcloud = 'SoundCloud';
        window.locale.sidebar.albums = 'Albumok';
}
